#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <glad\glad.h>
#include <GLFW\glfw3.h>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
int readShaderSource(GLuint shader, const char *file);
void printShaderInfoLog(GLuint shader);
void printProgramInfoLog(GLuint program);
void processInput(GLFWwindow *window);