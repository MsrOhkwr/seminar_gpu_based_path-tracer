#include "glsl.h"
#include <opencv2/opencv.hpp>

int main()
{
	fprintf(stdout, "Hello World!\n");

	if (!glfwInit())
	{
		fprintf(stderr, "Cannot initialize GLFW\n");
		glfwTerminate();
		return 1;
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	GLFWwindow* window = glfwCreateWindow(960, 540, "OpenGL", NULL, NULL);
	if (!window)
	{
		fprintf(stderr, "Cannot create window\n");
		return 1;
	}

	glfwMakeContextCurrent(window);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		fprintf(stderr, "Cannot initialize GLAD\n");
		glfwDestroyWindow(window);
		glfwTerminate();
		return 1;
	}

	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	int k = 1;
	const int tex_w = 960 * k, tex_h = 540 * k;

	GLuint tex_input;
	glGenTextures(1, &tex_input);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, tex_input);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tex_w, tex_h, 0, GL_RGBA, GL_FLOAT, NULL);
	glBindImageTexture(0, tex_input, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);

	GLuint tex_output;
	glGenTextures(1, &tex_output);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, tex_output);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tex_w, tex_h, 0, GL_RGBA, GL_FLOAT, NULL);
	glBindImageTexture(1, tex_output, 0, GL_FALSE, 0, GL_WRITE_ONLY, GL_RGBA32F);

	cv::Mat seedImg(cv::Size(tex_w, tex_h), CV_32FC4);

	for (int i = 0; i < tex_w; i++)
		for (int j = 0; j < tex_h; j++)
		{
			seedImg.at<cv::Vec4f>(j, i) =
				cv::Vec4f(
					float(rand()) / float(RAND_MAX),
					float(rand()) / float(RAND_MAX),
					float(rand()) / float(RAND_MAX),
					float(rand()) / float(RAND_MAX)
				);
		}
	
	GLuint seed;
	glGenTextures(1, &seed);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, seed);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, tex_w, tex_h, 0, GL_RGBA, GL_FLOAT, seedImg.data);
	glBindImageTexture(2, seed, 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32UI);

	GLuint hdri;
	glGenTextures(1, &hdri);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, hdri);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	static cv::Mat backImg = cv::imread("input/grace-new.hdr", -1);
	//static cv::Mat backImg = cv::imread("input/venice_dawn_1_2k.hdr", -1);
	//static cv::Mat backImg = cv::imread("input/syferfontein_0d_clear_2k.hdr", -1);
	//static cv::Mat backImg = cv::imread("input/studio_small_03_2k.hdr", -1);
	//static cv::Mat backImg = cv::imread("input/music_hall_02_2k.hdr", -1);

	cv::cvtColor(backImg, backImg, cv::COLOR_BGRA2RGBA);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, backImg.cols, backImg.rows, 0, GL_RGBA, GL_FLOAT, backImg.data);
	glBindImageTexture(3, hdri, 0, GL_FALSE, 0, GL_READ_ONLY, GL_RGBA32F);
	
	unsigned accumN = 1;
	GLuint accum;
	glGenBuffers(1, &accum);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, accum);
	glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(unsigned), &accumN, GL_DYNAMIC_DRAW);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, accum);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

	int success;

	// compute shader
	GLuint compute_shader = glCreateShader(GL_COMPUTE_SHADER);
	if (readShaderSource(compute_shader, "resource/compute_shader.glsl")) exit(1);
	glCompileShader(compute_shader);
	glGetShaderiv(compute_shader, GL_COMPILE_STATUS, &success);
	printShaderInfoLog(compute_shader);
	if (!success)
	{
		fprintf(stderr, "Compile error in compute shader.\n");
		exit(1);
	}
	GLuint ray_program = glCreateProgram();
	glAttachShader(ray_program, compute_shader);
	glLinkProgram(ray_program);
	glGetProgramiv(ray_program, GL_LINK_STATUS, &success);
	printProgramInfoLog(ray_program);
	if (!success)
	{
		fprintf(stderr, "link error in compute shader.\n");
		exit(1);
	}
	glDeleteShader(compute_shader);

	// vertex shader
	int vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	if (readShaderSource(vertex_shader, "resource/vertex_shader.glsl")) exit(1);
	glCompileShader(vertex_shader);
	// check for shader compile errors
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &success);
	printShaderInfoLog(vertex_shader);
	if (!success)
	{
		fprintf(stderr, "Compile error in vertex shader.\n");
		exit(1);
	}

	// fragment shader
	int fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	if (readShaderSource(fragment_shader, "resource/fragment_shader.glsl")) exit(1);
	glCompileShader(fragment_shader);
	// check for shader compile errors
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &success);
	printShaderInfoLog(fragment_shader);
	if (!success)
	{
		fprintf(stderr, "Compile error in fragment shader.\n");
		exit(1);
	}

	// link shaders
	int shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);
	glLinkProgram(shader_program);
	// check for linking errors
	glGetProgramiv(shader_program, GL_LINK_STATUS, &success);
	printProgramInfoLog(shader_program);
	if (!success) {
		fprintf(stderr, "Compile error in shader program\n");
		exit(1);
	}
	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	// vertiecs
	float vertices[] = {
		 1,  1, 0,  // top right
		 1, -1, 0,  // bottom right
		-1, -1, 0,  // bottom left
		-1,  1, 0,  // top left 
		 1,  1, 0,  // top right
	};

	unsigned int VBO, VAO;
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);

	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), NULL);
	glEnableVertexAttribArray(0);

	glClearColor(0, 0, 0, 1);

	while (!glfwWindowShouldClose(window) && accumN < 1 << 20)
	{
		processInput(window);

		glClear(GL_COLOR_BUFFER_BIT);

		glUseProgram(ray_program);
		glDispatchCompute((GLuint)tex_w, (GLuint)tex_h, 1);
		glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

		glUseProgram(shader_program);
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLE_STRIP, 0, 5);
        
		if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1))
		{
			accumN = 1;
            glBindBuffer(GL_SHADER_STORAGE_BUFFER, accum);
            glBufferData(GL_SHADER_STORAGE_BUFFER, sizeof(unsigned), &accumN, GL_DYNAMIC_COPY);
		}
        
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glBindTexture(GL_TEXTURE_2D, tex_input);
	float* gl_texture_bytes_f = (float*)malloc(sizeof(float) * tex_w * tex_h * 3);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_BGR, GL_FLOAT, gl_texture_bytes_f);

	glBindTexture(GL_TEXTURE_2D, tex_output);
	unsigned char* gl_texture_bytes_b = (unsigned char*)malloc(sizeof(unsigned char) * tex_w * tex_h * 3);
	glGetTexImage(GL_TEXTURE_2D, 0, GL_BGR, GL_UNSIGNED_BYTE, gl_texture_bytes_b);

	cv::Mat HDRI = cv::Mat(tex_h, tex_w, CV_32FC3, gl_texture_bytes_f);
	cv::Mat LDRI = cv::Mat(tex_h, tex_w, CV_8UC3, gl_texture_bytes_b);

	cv::flip(HDRI, HDRI, 0);
	cv::flip(LDRI, LDRI, 0);
	cv::imwrite("output/image.hdr", HDRI);
	cv::imwrite("output/image.bmp", LDRI);

	return 0;
}