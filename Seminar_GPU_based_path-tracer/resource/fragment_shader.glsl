#version 430 core

in vec2 st;
layout(binding = 1) uniform sampler2D scene_tex;
out vec4 frag_colour;

void main()
{
	frag_colour = texture (scene_tex, st);
}